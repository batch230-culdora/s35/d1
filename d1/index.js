const express = require("express");

// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [SECTION] MongoDB Connection
// Connect to the database by passing in your connection string, remember to replace the password and databasse names with actual values
// Syntax:
	// mongoose.connect("<MongoDB connection string>", {urlNewUrlParser: true})

mongoose.connect("mongodb+srv://admin:admin@batch230.5xn5745.mongodb.net/s35?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)
// Connection to database
// Allows to handle errors when the initial connection is establised
// works with the on and once Mongoose methods
let db = mongoose.connection

// If a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal

db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the cloud database"));

app.use(express.json());

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
	default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

app.post("/tasks", (request, response) => {
	// Check if there are duplicate task

	Task.findOne({name: request.body.name}, (error, result) => {

		// If there was found and the document's name matches the information via the client/Postman

		if(result!=null && result.name == request.body.name){
		return response.send("Duplicate task found");
		}
		else{

			let newTask = new Task({
				name: request.body.name
			})
			newTask.save((saveError, savedTask) => {

				if(saveError){
					return console.error(saveError);
				}

				else{
					return response.status(201).send("New task created");
				}
			})

		}
	})

});


app.get("/tasks", (request, response) =>{

	Task.find({}, (error, result) => {

		if(error){
			return console.log(error);
		}
		else{
			return response.status(200).json({
				data : result
			})
		}
	})
});

	
// ACTIVITY

/*1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a git repository named S35.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.*/

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})
const User = mongoose.model("User", userSchema);

app.post("/signup", (request, response) => {
	User.findOne({username: request.body.username}, (error, result) => {
		if(result!=null && result.username == request.body.username){
		return response.send("Existing Username");
		}
		else{

			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			})
			newUser.save((saveError, savedUser) => {

				if(saveError){
					return console.error(saveError);
				}

				else{
					return response.status(201).send("New user registered");
			}
			})

		}
	})

})


app.get("/signup", (request, response) =>{

	User.find({}, (error, result) => {

		if(error){
			return console.log(error);
		}
		else{
			return response.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port, ()=>console.log(`Server is running at port ${port}`));




